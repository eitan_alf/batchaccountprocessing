Account Batch

How to run AccountBatchClass
1. Open Developer Console in your org
2. Open Anonymous Apex
3. Fill one of the two versions code in the anonymous apex window:
	The code can run with two versions. One for solving the intial questions of updating corporate accounts with 'Bronze' value for 
	Enterprise_Account_Status__c field. Second version is the scalable solution to updating Enterprise_Account_Status__c field with a value based on a condition. 
	
	a. Database.executeBatch(new AccountBatchClass('', '', 'Bronze'));

	b. Database.executeBatch(new AccountBatchClass('Gold_Account__c', true, 'Gold'));

